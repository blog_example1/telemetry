use actix_web::{get, web, App, HttpResponse, HttpServer};

use serde::{Deserialize, Serialize};
use serde_json::json;
use sqlite::{Connection, State};
use thiserror::Error;
use tracing_subscriber::layer::SubscriberExt;
use tracing_subscriber::util::SubscriberInitExt;
use tracing_subscriber::EnvFilter;

#[derive(Error, Debug)]
enum CustomError {
    #[error("sqlite errors {0:?}")]
    Sqlite(sqlite::Error),
    #[error("reqwest errors")]
    Reqwest(reqwest::Error),
    #[error("json errors")]
    Json(serde_json::Error),
}

impl From<sqlite::Error> for CustomError {
    fn from(err: sqlite::Error) -> Self {
        CustomError::Sqlite(err)
    }
}

impl From<serde_json::Error> for CustomError {
    fn from(err: serde_json::Error) -> Self {
        CustomError::Json(err)
    }
}

impl From<reqwest::Error> for CustomError {
    fn from(err: reqwest::Error) -> Self {
        CustomError::Reqwest(err)
    }
}

type Result<T> = std::result::Result<T, CustomError>;

#[derive(Deserialize, Serialize)]
struct User {
    id: u32,
    name: String,
    email: String,
    phone: String,
    website: String,
}

struct Cache;

impl Cache {
    #[tracing::instrument]
    fn init() -> Result<Connection> {
        let connection = sqlite::open("db.sqlite")?;
        connection.execute("CREATE TABLE IF NOT EXISTS users (id INTEGER , value TEXT )")?;

        Ok(connection)
    }

    #[tracing::instrument(skip(user), fields(id = user.id))]
    fn set(user: &User) -> Result<()> {
        let connection = Self::init()?;
        let mut statement = connection.prepare("INSERT INTO users VALUES(?, ?)")?;
        let user_string = json!(user).to_string();
        statement.bind(1, user.id as i64)?;
        statement.bind(2, user_string.as_str())?;

        statement.next()?;

        Ok(())
    }

    #[tracing::instrument]
    async fn hit(key: u32) -> Result<Option<User>> {
        let connection = Self::init()?;

        let mut statement = connection.prepare("SELECT * FROM users WHERE id = ?")?;
        statement.bind(1, key as i64)?;

        match statement.next().unwrap() {
            State::Row => {
                let user_string = statement.read::<String>(1)?;
                let user = serde_json::from_str::<User>(&user_string)?;

                Ok(Some(user))
            }
            State::Done => {
                let user = fetch_user(key).await?;
                Self::set(&user)?;

                Ok(Some(user))
            }
        }
    }
}

#[tracing::instrument]
async fn fetch_user(id: u32) -> Result<User> {
    let url = format!("https://jsonplaceholder.typicode.com/users/{}", id);

    let response = reqwest::get(url).await?;
    let response_string = response.text().await?;
    let user = serde_json::from_str(response_string.as_str())?;

    Ok(user)
}

#[get("/user/{id}")]
#[tracing::instrument]
async fn get_user(id: web::Path<u32>) -> HttpResponse {
    let id = id.into_inner();

    if id > 10 {
        return HttpResponse::BadRequest().json(json!({
            "message" : "User id can't exceed 10"
        }));
    }

    let user = Cache::hit(id).await;

    match user {
        Ok(user) => HttpResponse::Ok().json(user),
        Err(err) => HttpResponse::NotFound().json(json!({
           "code": 404,
            "message": err.to_string(),
        })),
    }
}

#[actix_web::main] // or #[tokio::main]
#[tracing::instrument]
async fn main() -> std::io::Result<()> {
    // Create a Jaeger Tracer
    let tracer_jaeger = opentelemetry_jaeger::new_pipeline()
        .with_service_name("cache user")
        .install_simple()
        .unwrap();

    // Create a tracing layer with the configured tracer
    let opentelemetry_layer = tracing_opentelemetry::layer()
        .with_tracer(tracer_jaeger)
        .with_threads(true);

    // Register Layers
    tracing_subscriber::registry()
        .with(EnvFilter::from_default_env())
        .with(opentelemetry_layer)
        .init();

    HttpServer::new(|| App::new().service(get_user))
        .bind(("127.0.0.1", 8080))?
        .run()
        .await
}
